﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperSpeed : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PlayerItemCol"))
        {
            other.GetComponentInParent<PlayerMovement>().GetSuperSpeed();

            Destroy(this.gameObject);
        }
    }
}
