﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recovery : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PlayerItemCol"))
        {
            other.GetComponentInParent<PlayerHealth>().RecoverFullHealth();

            Destroy(this.gameObject);
        }
    }
}
